class Poker:
  def hand_rank(self, hand):
    ranks = self.card_ranks(hand)
    if self.straight(ranks) and self.flush(hand):       # straight flush
      return (5, max(ranks))
    elif self.flush(hand):                              # flush
      return (4, max(ranks))
    elif self.straight(ranks):                          # straight
      return (3, max(ranks))
    elif self.kind(3, ranks):                           # 3 of a kind
      return (2, self.kind(3, ranks))
    elif self.kind(2, ranks):                           # 2 kinds
      return (1, self.kind(2, ranks), self.kind(1, ranks))
    else:                                               # high card
      return (0, sum(ranks))

  #Get the rank of each card itself
  def card_ranks(self, cards):
    ranks = ['--23456789TJQKA'.index(r) for r,s in cards]
    ranks.sort(reverse = True)
    return ranks

  #Checks if there's a 3 card straight 
  def straight(self, ranks):
    return (max(ranks) - min(ranks) == 2) and len(set(ranks)) == 3

  #Returns true if all cards have same suit
  def flush(self, hand):
    suits = [s for r, s in hand]
    return len(set(suits)) == 1

  def kind(self, n,ranks):
    for r in ranks:
      if ranks.count(r) == n: return r
    return None
