Three Card Poker Judger
==================================

Python script to judge a game of poker, each hand having three cards.

How to Run
----------
* Run `python game.py`
* Enter any integer to represent the number of players
* Enter the ID of the player, followed by 3 cards that represent `<rank><Suit>`, separated by spaces. The number of lines prompted will depend on the number of players entered.

How to Run Tests
----------------
* Run `run_tests python game.py`

Notes & Limitations
-----------
* This program was written using Python 2.7.13
* I do not include any handling of invalid input. For example, `<rank>` must be a captial letter, `<Suit>` must be lowercase, as well as be valid cards themselves. This is just a bare bones implementation to demonstrate how to solve the problem.

Input Format
------------

Input is read over `stdin` and cards will be represented in the format `<rank><Suit>`.

Rank will be one of:

* an integer from `2` to `9` for numbered cards less than ten
* `T` for ten
* `J` for jack
* `Q` for queen
* `K` for king
* `A` for ace

Suit will be one of:

* `h` for hearts
* `d` for diamonds
* `s` for spades
* `c` for clubs

Examples: `4d` for four of diamonds, `Ts` for ten of spades, and `Ah` for ace of hearts.