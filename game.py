#!/usr/bin/env python

import sys
from poker import Poker

player_count = sys.stdin.readline()
count = int(player_count)
players = {}
game = Poker()

for i in range(0, count, 1):
  player_input = sys.stdin.readline()
  player_data = player_input.split()
  players[player_data[0]] = game.hand_rank(player_data[1:])

winners = [str(i) for i, x in enumerate(players.values()) if x == max(players.values())]
print " ".join(winners)